package com.libgdx.my_game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.dcostap.engine.utils.Animation;
import com.kotcrab.vis.ui.widget.VisLabel;

/**
 * Created by Darius on 17/01/2018
 */
public class AnimationActor extends Image {
	MyGame.AnimInfo animInfo;
	private int scale;
	private float frameDelta;
	VisLabel framesLabel;

	boolean paused = false;
	boolean onion = false;

	public AnimationActor(MyGame.AnimInfo animInfo, int scale) {
		super(animInfo.anim.getFrame());
		this.validate();
		this.animInfo = animInfo;
		this.scale = scale;

		this.getDrawable().setMinWidth(getImageWidth() * scale);
		this.getDrawable().setMinHeight((getImageHeight() * scale) + 25f);
	}

	@Override
	public void act(float delta) {
		super.act(delta);
		frameDelta = delta;
	}


	public void reverseAnimationToggle() {
		if (animInfo.anim.getAnimType() == Animation.AnimType.REVERSED) {
			animInfo.anim.setAnimType(Animation.AnimType.LOOP);
		} else {
			animInfo.anim.setAnimType(Animation.AnimType.REVERSED);
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		Texture texture;
		if (!paused) animInfo.anim.update(frameDelta);
		texture = animInfo.anim.getFrame();

		if (paused && onion) {
			batch.setColor(0f, 0f, 0f, 0.5f);
			int previous = animInfo.anim.getCurrentFrame() - 1;
			if (previous < 0) previous = animInfo.anim.getNumberOfFrames() - 1;
//			batch.draw(animInfo.anim.getFrames().get(previous), getX(), getY(), texture.getWidth() * scale, texture.getHeight() * scale);
			batch.setColor(Color.WHITE);
		}

		batch.draw(texture, getX(), getY(), texture.getWidth() * scale, texture.getHeight() * scale);

		float margin = 10f;
		batch.setColor(0f, 0f, animInfo.onFocus ? 0.4f : 0.1f, animInfo.onFocus ? 0.27f : 0.12f);
		drawRectangle(batch, getX() - margin, getY() - margin, (margin * 2) + texture.getWidth() * scale, (margin * 2) + texture.getHeight() * scale, 3f);
		drawRectangle(batch, getX() - margin, getY() - margin, (margin * 2) + texture.getWidth() * scale, (margin * 2) + texture.getHeight() * scale, margin);
		batch.setColor(Color.WHITE);
	}

	private static Texture pixel = new Texture("atlas/textures_finished/pixel.png");

	private void drawRectangle(Batch batch, float x, float y, float width, float height, float thickness) {
		batch.draw(pixel, thickness + x, y, 0f, 0f, width - thickness * 2, thickness, 1f, 1f, 0f, 0, 0, pixel.getWidth(), pixel.getHeight(), false, false);
		batch.draw(pixel, x, y, 0f, 0f, thickness, height, 1f, 1f, 0f, 0, 0, pixel.getWidth(), pixel.getHeight(), false, false);
		batch.draw(pixel, x + thickness, y + height - thickness, 0f, 0f, width - thickness * 2, thickness, 1f, 1f, 0f, 0, 0, pixel.getWidth(), pixel.getHeight(), false, false);
		batch.draw(pixel, x + width - thickness, y, 0f, 0f, thickness, height, 1f, 1f, 0f, 0, 0, pixel.getWidth(), pixel.getHeight(), false, false);
	}
}
