package com.libgdx.my_game

import com.badlogic.gdx.graphics.Texture
import com.dcostap.engine.utils.Animation
import com.kotcrab.vis.ui.widget.VisLabel

/** Created by Darius on 22-Feb-20. */
class FrameNumberLabel(val animation: Animation<Texture>) : VisLabel() {
	override fun act(delta: Float) {
		super.act(delta)

		setText("${animation.currentFrame}/${animation.numberOfFrames - 1}")
	}
}