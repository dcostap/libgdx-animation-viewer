package com.libgdx.my_game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.dcostap.engine.utils.Animation;
import com.dcostap.engine.utils.ExportedImagesProcessor;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisWindow;
import com.kotcrab.vis.ui.widget.color.ColorPicker;
import com.kotcrab.vis.ui.widget.color.ColorPickerAdapter;
import com.kotcrab.vis.ui.widget.file.FileChooser;
import com.kotcrab.vis.ui.widget.file.FileChooserAdapter;
import kotlin.text.MatchGroup;
import kotlin.text.Regex;

import java.awt.*;
import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyGame extends Game {
	private String preferencesName = "com.animationViewer";

	public static int BASE_WIDTH = 500;
	public static int BASE_HEIGHT = 450;

	private SpriteBatch batch;
	private Stage stage;

	private Table parentTable;
	private Label pathField;
	private Button confirmPath;
	private ScrollPane animationsViewScroll;
	private Table animationsView;

	private Color backgroundColor = new Color(0.51f, 0.62f, 0.65f, 1);

	private float defaultSpeed = 7f;
	private Label defaultSpeedLabel;
	private Label filesFoundLabel;

	private TextButton hideButtonsButton;
	private TextButton searchRecursiveButton;

	private Skin skin;
	private ObjectSet<FileHandle> images = new ObjectSet<>();
	private ArrayList<AnimInfo> animations = new ArrayList<>();

	private int scale = 3;
	private boolean hideButtons = false;

	private boolean searchRecursive = true;

	private VisWindow options;

	private boolean watch = true;
	private boolean packageOnWatch = true;

	public void optionsWindow() {
		if (options.getStage() != null) return;
		options = new VisWindow("Options");
		stage.addActor(options);
		options.addCloseButton();

		NinePatchDrawable d = (NinePatchDrawable) VisUI.getSkin().getDrawable("window");
		NinePatch p = new NinePatch(d.getPatch());
		NinePatchDrawable dp = new NinePatchDrawable(p);
		dp = dp.tint(Color.WHITE);
		options.setBackground(dp);

		Table tf = new Table();
		TextButton b = new TextButton("Background color", skin);
		tf.add(b).pad(2);

		ColorPicker picker = new ColorPicker(new ColorPickerAdapter() {
			@Override
			public void finished(Color newColor) {
				backgroundColor = newColor;
			}
		});

		b.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				picker.setColor(backgroundColor);
				stage.addActor(picker);
			}
		});
		tf.row();

		hideButtonsButton = new CheckBox("Hide buttons", skin);
		hideButtonsButton.setChecked(hideButtons);
		hideButtonsButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				hideButtons = !hideButtons;

				loadAnimations();
			}
		});
		Table thirdRow = new Table();
		tf.add(thirdRow).colspan(222);
		tf.setColor(new Color(0f, 0.3f, 0.5f, 1));

		thirdRow.add(hideButtonsButton);

		searchRecursiveButton = new CheckBox("Search path recursively", skin);
		searchRecursiveButton.setChecked(searchRecursive);
		searchRecursiveButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				searchRecursive = !searchRecursive;
			}
		});
		thirdRow.row();
		thirdRow.add(searchRecursiveButton).padLeft(15);

		thirdRow.row();
		CheckBox watchB = new CheckBox("Watch folder", skin);
		watchB.setChecked(watch);
		watchB.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				watch = !watch;
			}
		});
		thirdRow.add(watchB).padLeft(15);

		thirdRow.row();
		CheckBox watchP = new CheckBox("Package whenever watch detects changes\n(instead of only update)", skin);
		watchP.setChecked(packageOnWatch);
		watchP.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				packageOnWatch = !packageOnWatch;
			}
		});
		thirdRow.add(watchP).padLeft(15);

		options.add(tf);
		options.pack();
		options.centerWindow();
	}

	@Override
	public void create() {
		Preferences pref = Gdx.app.getPreferences(preferencesName);
		MyGame.BASE_WIDTH = pref.getInteger("width", 500);
		MyGame.BASE_HEIGHT = pref.getInteger("height", 450);

		Gdx.app.getGraphics().setWindowedMode(MyGame.BASE_WIDTH, MyGame.BASE_HEIGHT);

		FileChooser.setDefaultPrefsName(preferencesName + ".fileChooser");

		batch = new SpriteBatch();
		stage = new Stage(new ScreenViewport());
		VisUI.load();
		options = new VisWindow("Options");
		((VisWindow) options).addCloseButton();
		options.pack();
		skin = VisUI.getSkin();

		parentTable = new Table();
		parentTable.setFillParent(true);
		stage.addActor(parentTable);

		Table tf = new VisTable();

		tf.setBackground(VisUI.getSkin().getDrawable("button-over"));
		TextButton b = new TextButton("Options", VisUI.getSkin());
		b.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				optionsWindow();
			}
		});
		tf.add(b);

		Label changeScaleLabel = new Label("Scale: " + scale, skin);
		TextButton less = new TextButton("-", skin);
		less.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				scale--;
				if (scale < 1) scale = 1;
				changeScaleLabel.setText("Scale: " + scale);
				loadPath(pathField.getText().toString());
			}
		});

		TextButton moar = new TextButton("+", skin);
		moar.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				scale++;
				changeScaleLabel.setText("Scale: " + scale);
				loadPath(pathField.getText().toString());
			}
		});

		Table scale = new Table();
		scale.add(changeScaleLabel).pad(0, 10, 0, 10);
		scale.add(moar, less);
		tf.add(scale);

		defaultSpeedLabel = new Label("", skin);
		updateDefaultSpeedLabel();

		TextButton slower = new TextButton("-", skin);
		slower.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				defaultSpeed -= 0.7f;
				updateDefaultSpeedLabel();
			}
		});

		TextButton faster = new TextButton("+", skin);
		faster.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				defaultSpeed += 0.7f;
				if (defaultSpeed < 0f) defaultSpeed = 0f;
				updateDefaultSpeedLabel();
			}
		});

		tf.add(defaultSpeedLabel).padLeft(5f);
		tf.add(faster).maxHeight(20);
		tf.add(slower).maxHeight(20);

//        filesFoundLabel = new Label("", skin);
//        tf.add(filesFoundLabel).pad(0, 10, 0, 0);

		tf.row();

		parentTable.add(tf).expandX().fillX();
		parentTable.row();

		animationsView = new Table();
		animationsViewScroll = new ScrollPane(animationsView, VisUI.getSkin());
		parentTable.add(animationsViewScroll).fill().expand().colspan(5);

		animationsViewScroll.setFadeScrollBars(false);
		animationsViewScroll.setScrollingDisabled(true, false);
		animationsViewScroll.setFlickScroll(false);

		parentTable.row();

		pathField = new Label("", skin);
		Table downTable = new Table();
		parentTable.add(downTable).fillX().expandX().bottom();

		Table pathFieldT = new Table();
		ScrollPane sp = new ScrollPane(pathFieldT, VisUI.getSkin());
		pathFieldT.setBackground(VisUI.getSkin().getDrawable("button"));
//        ScrollPane pane = new ScrollPane(pathFieldT, VisUI.getSkin());
		downTable.add(sp).fillX().expandX().height(25);
//        pane.setScrollingDisabled(true, true);
		pathFieldT.add(pathField);

		FileChooser fileChooser = new FileChooser(FileChooser.Mode.OPEN);
		fileChooser.setSelectionMode(FileChooser.SelectionMode.DIRECTORIES);
		fileChooser.setHeight(Gdx.graphics.getHeight());
		fileChooser.setWidth(Gdx.graphics.getWidth());

		fileChooser.setListener(new FileChooserAdapter() {
			@Override
			public void selected(Array<FileHandle> file) {
				pathField.setText(file.first().path());
				loadPath(pathField.getText().toString());
			}
		});

		Button explorer = new TextButton("Explorer", skin);
		Button delete = new TextButton("Delete", skin);
		Button pack = new TextButton("Package", skin);
		Button update = new TextButton("Update", skin);

		Table downButtonsTable = new Table();
		downTable.row();
		downTable.add(downButtonsTable).growX();
		downButtonsTable.defaults().growX();
		explorer.setColor(Color.CYAN);
		explorer.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				try {
					Desktop.getDesktop().open(new File(pathField.getText().toString()));
				} catch (Exception exc) {

				}
			}
		});
		downButtonsTable.add(explorer);

		delete.setColor(Color.RED);
		delete.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				boolean ellipsis = pathField.getText().length > 42 - 3;
				String added = ellipsis ? "..." : "";
				String path = pathField.getText().toString().substring(Math.max(pathField.getText().length - 42, 0), pathField.getText().length);
				Window window = new Window("Are you sure?", VisUI.getSkin());
				window.setModal(true);
				stage.addActor(window);

				window.add(new VisLabel("Delete all .png images inside \n" + added + path + " ?"));
				window.row();

				Table table = new Table();
				window.add(table);
				table.add(new VisTextButton("YES", new ChangeListener() {
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						deleteFiles(pathField.getText().toString());
						window.remove();
						update.toggle();
					}
				}));
				table.add(new VisTextButton("NO", new ChangeListener() {
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						window.remove();
					}
				})).padLeft(30f);

				window.pack();
				window.center();
			}
		});


		downButtonsTable.add(delete).fill();

		pack.setColor(Color.YELLOW);
		pack.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				packageFolder();
				update.toggle();
			}
		});
		downButtonsTable.add(pack).fill();

		update.setColor(Color.BLUE);
		update.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				loadPath(pathField.getText().toString());
			}
		});
		downButtonsTable.add(update).fill();

		Button clear = new TextButton("Browse...", skin);
		clear.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				fileChooser.setDirectory(pathField.getText().toString());
				stage.addActor(fileChooser);
			}
		});
		downButtonsTable.add(clear).fill();

//        confirmPath = new TextButton("Look path", skin);
//        downTable.add(confirmPath);
//        confirmPath.addListener(new ChangeListener() {
//            @Override
//            public void changed(ChangeEvent event, Actor actor) {
//                loadPath(pathField.getText().toString());
//            }
//        });

		Gdx.input.setInputProcessor(stage);

		// load saved file path, if it exists
		if (pref.contains("path")) {
			pathField.setText(pref.getString("path", ""));
			try {
				loadPath(pathField.getText().toString());
			} catch (Exception ex) {

			}
		}
	}

	private void deleteFiles(String path) {
		for (FileHandle entry : Gdx.files.absolute(path).list()) {
			if (entry.isDirectory() && searchRecursive) {
				deleteFiles(entry.path());

				try {
					entry.delete();
				} catch (Exception exc) {

				}
			} else if (entry.extension().equals("png")) {
				entry.delete();
			}
		}
	}

	private void packageFolder() {
		ExportedImagesProcessor.Companion.setImagesOrigin(pathField.getText().toString());
		ExportedImagesProcessor.Companion.processExportedImages();
	}

	private void updateDefaultSpeedLabel() {
		defaultSpeedLabel.setText("Def. frames/s: " + String.format(Locale.ENGLISH, "%.2f",
				defaultSpeed) + "");

		if (pathField != null)
			loadPath(pathField.getText().toString());
	}

	private void loadPath(String path) {
		images.clear();

		try {
			searchFolder(path, images);
		} catch (Exception exc) {

		}

		lastModf = Gdx.files.absolute(path).lastModified();

		loadAnimations();
	}

	private void searchFolder(String path, ObjectSet<FileHandle> images) {
		try {
			for (FileHandle entry : Gdx.files.absolute(path).list()) {
				if (entry.isDirectory() && searchRecursive) {
					searchFolder(entry.path(), images);
				} else if (entry.extension().equals("png")) {
					images.add(entry);
				}
			}
		} catch (Exception exc) {

		}
	}

	@Override
	public void pause() {
		Preferences pref = Gdx.app.getPreferences(preferencesName);
		pref.putString("path", pathField.getText().toString());
		pref.putInteger("width", Gdx.app.getGraphics().getWidth());
		pref.putInteger("height", Gdx.app.getGraphics().getHeight());
		pref.flush();

		super.pause();
	}

	private class Frame {
		private FileHandle file;
		private int frameNumber;
	}

	static class AnimInfo {
		Animation<Texture> anim;
		boolean onFocus = false;
		String name;
	}

	private void loadAnimations() {
		ArrayList<AnimInfo> oldAnims = animations;
		animations = new ArrayList<>();
		ObjectMap<String, Array<Frame>> uniqueAnimations = new ObjectMap<>();
		for (FileHandle fileHandle : images) {
			if (fileHandle.nameWithoutExtension().matches(".*_n\\d*")) {
				// dupe code
				Pattern p = Pattern.compile("-(\\d+)=([+-]?([0-9]*[.])?[0-9]+)");
				Matcher m = p.matcher(fileHandle.nameWithoutExtension());
				String name = fileHandle.nameWithoutExtension();
				ArrayList<String> replaces = new ArrayList<>();
				while (m.find()) {
					replaces.add(m.group(0));
					System.out.println("----> " + m.group(0));
				}

				String newName = name;
				for (String r : replaces) {
					newName = newName.replace(r, "");
				}

				int ind = newName.lastIndexOf("_n");
				if (ind >= 0)
					newName = newName.substring(0, ind);

				String key = newName;

				Array<Frame> anim = uniqueAnimations.get(key, new Array<>());
				String indexIndicator = fileHandle.nameWithoutExtension().substring(fileHandle.nameWithoutExtension().lastIndexOf("_n") + 2);
				int index = Integer.parseInt(indexIndicator);

				Frame frame = new Frame();
				frame.frameNumber = index;
				frame.file = fileHandle;
				anim.add(frame);
				uniqueAnimations.put(key, anim);
			}
		}

		for (Array<Frame> anim : uniqueAnimations.values()) {
			Array<Texture> textures = new Array<Texture>();
			anim.sort((o1, o2) -> {
				if (o1.frameNumber < o2.frameNumber) return -1;
				else if (o1.frameNumber == o2.frameNumber) return 0;
				else return 1;
			});

			String animName = anim.get(0).file.nameWithoutExtension().substring(0,
					anim.get(0).file.nameWithoutExtension().lastIndexOf("_n"));

			for (Frame frame : anim) {
				Pattern p = Pattern.compile("-(\\d+)=([+-]?([0-9]*[.])?[0-9]+)");
				Matcher m = p.matcher(frame.file.nameWithoutExtension());
				float mult = 1f;
				String name = frame.file.nameWithoutExtension();
				String newName = name;
				while (m.find()) {
					 newName = newName.replace(m.group(0), "");
					if (Integer.parseInt(m.group(1)) == frame.frameNumber) mult = Float.parseFloat(m.group(2));
				}

				animName = newName.substring(0, newName.lastIndexOf("_n"));

				textures.add(new AnimFrameRegion(mult, frame.file));
			}

			AnimInfo animInfo = new AnimInfo();
			animInfo.name = animName;

			float finalSpeed = 1f / defaultSpeed;
			AnimInfo old = null;
			for (AnimInfo oldAnim : oldAnims) {
				if (oldAnim.name.equals(animName)) old = oldAnim;
			}
			boolean onFocus = false;
			if (old != null) {
				finalSpeed = old.anim.getFrameDuration();
				onFocus = old.onFocus;
			}

			Animation<Texture> animation = new Animation<>(textures, finalSpeed);
			animInfo.anim = animation;
			animInfo.onFocus = onFocus;
			animations.add(animInfo);
		}

		animationsView.clearChildren();

		animations.sort(Comparator.comparing(o -> o.name));
		ArrayList<AnimationActor> allActors = new ArrayList<>();
		String tt = pathField.getText().toString().substring(Math.max(pathField.getText().length - 20, 0), pathField.getText().length);
		Gdx.graphics.setTitle("" + animations.size() + " anims found in ..." + tt);
		for (AnimInfo animation : animations) {
			Table table = new Table();
			Table window = new Table(skin);
			AnimationActor animationActor = new AnimationActor(animation, scale);
			allActors.add(animationActor);
			table.center();
			Table ttt = new Table();
			ttt.setTouchable(Touchable.enabled);

			ttt.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					super.clicked(event, x, y);

					for (AnimationActor actor : allActors) actor.animInfo.onFocus = false;
					animationActor.animInfo.onFocus = true;
				}
			});

			Table frameChooserTable = new Table();
			VisLabel frames = new FrameNumberLabel(animation.anim);
			frames.setColor(Color.BLACK);
			frameChooserTable.right().top();

			TextButton previousFrame = new TextButton("-", skin);
			previousFrame.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					int newFrame = animationActor.animInfo.anim.getCurrentFrame();
					if (animationActor.paused) {
						newFrame--;
						if (newFrame < 0) newFrame = animationActor.animInfo.anim.getNumberOfFrames() - 1;
					}

					animationActor.paused = true;
					animationActor.animInfo.anim.setCurrentFrame(newFrame);
				}
			});

			TextButton nextFrame = new TextButton("+", skin);
			nextFrame.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					int newFrame = animationActor.animInfo.anim.getCurrentFrame();
					if (animationActor.paused) {
						newFrame++;
						if (newFrame >= animationActor.animInfo.anim.getNumberOfFrames()) newFrame = 0;
					}

					animationActor.paused = true;
					animationActor.animInfo.anim.setCurrentFrame(newFrame);
				}
			});

			TextButton resume = new TextButton("unpause", skin) {
				boolean forwardPressed = false;
				boolean backPressed = false;

				@Override
				public void act(float delta) {
					super.act(delta);

					setVisible(animationActor.paused);

					previousFrame.setProgrammaticChangeEvents(true);
					nextFrame.setProgrammaticChangeEvents(true);
					if (animationActor.animInfo.onFocus) {
						if (Gdx.input.isButtonPressed(Input.Buttons.BACK)) {
							if (!backPressed) {
								previousFrame.toggle();
							}
							backPressed = true;
						} else {
							backPressed = false;
						}
						if (Gdx.input.isButtonPressed(Input.Buttons.FORWARD)) {
							if (!forwardPressed) {
								nextFrame.toggle();
							}
							forwardPressed = true;
						} else {
							forwardPressed = false;
						}
					}
				}
			};

			resume.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					animationActor.paused = false;
				}
			});

			TextButton onion = new TextButton("onion", skin) {
				@Override
				public void act(float delta) {
					super.act(delta);

					setVisible(animationActor.paused);
				}
			};
			onion.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					animationActor.onion = !animationActor.onion;
				}
			});

			frameChooserTable.add(onion).maxHeight(20).padRight(10f);
			frameChooserTable.add(resume).maxHeight(20).padRight(10f);
			frameChooserTable.add(previousFrame).maxHeight(20).width(16);
			frameChooserTable.add(frames).right();
			frameChooserTable.add(nextFrame).maxHeight(20).width(16);

			ttt.add(animationActor);
			Stack stack = new Stack(ttt, frameChooserTable);
			table.add(stack).fill().expand().pad(3).center().colspan(2);
			table.row();
			if (!hideButtons) table.add(window).padTop(8f);
			window.setBackground(VisUI.getSkin().getDrawable("button-over"));
			window.add(new Label(animation.name, skin)).colspan(5);
			window.row();
			Label speedText = new Label("" + String.format(Locale.ENGLISH, "%.2f",
					1f / animationActor.animInfo.anim.getFrameDuration()) + " frames/s", skin);
			TextButton slower = new TextButton("-", skin);
			slower.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					if (1 / animationActor.animInfo.anim.getFrameDuration() > 0.7f)
						animationActor.animInfo.anim.setFrameDuration(1 / ((1 / animationActor.animInfo.anim.getFrameDuration()) - 0.7f));
					speedText.setText("" + String.format(Locale.ENGLISH, "%.2f",
							1 / animationActor.animInfo.anim.getFrameDuration()) + " frames/s");
					animationActor.paused = false;
				}
			});

			TextButton faster = new TextButton("+", skin);
			faster.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					animationActor.animInfo.anim.setFrameDuration(1 / ((1 / animationActor.animInfo.anim.getFrameDuration()) + 0.7f));

					speedText.setText("" + String.format(Locale.ENGLISH, "%.2f",
							1 / animationActor.animInfo.anim.getFrameDuration()) + " frames/s");
					animationActor.paused = false;
				}
			});
			window.center();
			Table tttt = new Table();
			window.add(tttt).center().fillX().expandX();

			tttt.add(faster).maxHeight(20);
			tttt.add(slower).maxHeight(20);
			tttt.add(speedText);
			CheckBox reverseAnim = new CheckBox("Reversed", skin);
			reverseAnim.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					if (!reverseAnim.isChecked()) {
						animationActor.reverseAnimationToggle();
					} else {
						animationActor.reverseAnimationToggle();
					}
				}
			});
			tttt.add(reverseAnim).pad(0, 10, 0, 0);
//            window.setMovable(false);

			table.setClip(true);
			animationsView.add(table);
			animationsView.row();

			stage.setScrollFocus(animationsViewScroll);
		}
	}

	private long lastModf = -1L;
	private float updateTime = 0f;
	private float waitTime = 0f;

	@Override
	public void render() {
		if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE))
			Gdx.app.exit();
		if (Gdx.input.isKeyPressed(Input.Keys.R)) {
			dispose();
			create();
		}
		stage.act();

		Gdx.gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.draw();

		if (watch) {
			updateTime += Gdx.graphics.getDeltaTime();
			if (updateTime > 0.7f) {
				updateTime = 0f;

				long mod = Gdx.files.absolute(pathField.getText().toString()).lastModified();
				if (mod != lastModf) {
					System.out.println("changes");
					lastModf = mod;
					waitTime = 0f;
				} else {
					if (waitTime >= 0f) {
						waitTime += 0.7f;

						if (waitTime > 0.6f) {
							waitTime = -1f;
							if (packageOnWatch) packageFolder();
							loadPath(pathField.getText().toString());
						}
					}
				}
			}
		}
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void dispose() {
		batch.dispose();
		VisUI.dispose();
	}
}
