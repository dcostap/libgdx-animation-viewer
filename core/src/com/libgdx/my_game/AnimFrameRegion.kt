package com.libgdx.my_game

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion

class AnimFrameRegion(val frameSpeedMultiplier: Float, file: FileHandle?) : Texture(file)