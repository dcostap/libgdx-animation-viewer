package com.libgdx.my_game.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.libgdx.my_game.MyGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = MyGame.BASE_WIDTH;
		config.height = MyGame.BASE_HEIGHT;
		config.vSyncEnabled = false;
        config.foregroundFPS = 60;
        config.backgroundFPS = 60;

        config.title = "com.libgdx.my_game.Animation Viewer";
        config.addIcon("atlas/textures_finished/icon1.png", Files.FileType.Internal);
        config.addIcon("atlas/textures_finished/icon2.png", Files.FileType.Internal);
        config.addIcon("atlas/textures_finished/icon3.png", Files.FileType.Internal);

		new LwjglApplication(new MyGame(), config);
	}
}
